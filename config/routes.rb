Rails.application.routes.draw do
  use_doorkeeper
  namespace :api, defaults: {format: 'json'}  do
    # /api/ ... Api::
    namespace :v1 do
      resources :bumblebees
    end
  end
  root                      'static_pages#home'
  post 'help' => 'static_pages#help'
  get     'help'        =>  'static_pages#help'
  get     'about'       =>  'static_pages#about'
  get     'contact'     =>  'static_pages#contact'
  get     'signup'      =>  'users#new'
  get     'login'       =>  'sessions#new'
  post    'login'       =>  'sessions#create'
  delete  'logout'      =>  'sessions#destroy'
  get     'unconfirmed' =>  'bumblebees#unconfirmed'
  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  # resources :bumblebees,          only: [:create, :destroy, :edit, :update]
  resources :bumblebees
end
