require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper

  def setup
    @user = users(:michael)
  end

  # Visit user's profile page (My Bumblebees)
  # check for the page title and user's name, gravatar, bumblebee count,
  # and paginated bumblebees
  test "profile display" do
    get user_path(@user)
    assert_template 'users/show'
    assert_select 'title', full_title(@user.name)
    assert_select 'h1', text: @user.name
    assert_select 'h1>img.gravatar'
    assert_match @user.bumblebees.count.to_s, response.body
    assert_select 'div.pagination'
    @user.bumblebees.paginate(page: 1).each do |bumblebee|
      assert_match bumblebee.description, response.body
    end
  end
end
