require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  # Get edit path
  # Get users/edit view
  # Patch because it's in a DB, update with incorrect data
  # Get users/edit view
  test "unsuccessful edit" do
    # after adding restriction for logged in users before they can edit 
    # other users we must first login user 'log_in_as(@user)'so test can pass
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), user: { name: "",
                                    email: "fo@invalid",
                                    password:              "f0",
                                    password_confirmation: "bar" }
    assert_template 'users/edit'
  end

  # Successful edit with friendly forwading (reversing the order of logging in) 
  # and visiting the edit page:
  # Try to visit edit page
  # Logs in
  # checks that the user is redirected to edit page instead of default profile page
  # Update with correct data
  # Check for a nonempty flash message
  # Check for successfull redirect to profile page
  # Check for user's information correctly changed in DB
  test "successful edit with friendly forwarding" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    name  = "Foo Bar"
    email = "foo@bar.com"
    patch user_path(@user), user: { name:  name,
                                    email: email,
                                    password:              "",
                                    password_confirmation: "" }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal @user.name,  name
    assert_equal @user.email, email
  end
end
