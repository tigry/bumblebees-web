require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest

# visit login path
# verify the new sessions form renders properly
# post to the sessions path with invalid parms hash
# verify the new sessions form gets re-rendered and that a flash message appears
# visit another page (such as home page)
# verify that the flash message doesn't appear on the new page

  test "login with invalid information" do
    get login_path 
    assert_template 'sessions/new' 
    post login_path, session: { email: "", password: "" } 
    assert_template 'sessions/new' 
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end

  def setup
    @user = users(:michael)
  end

  test "login with valid information followd by logout" do
    get login_path
    post login_path, session: { email: @user.email, password: 'password' }
    assert is_logged_in?
    assert_redirected_to @user # check the right redirect target
    follow_redirect! # visit the target page
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count: 0 # verify that login link dissapears by verifying there are zero login path links on the page
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)
    # after logging in we use delete to issue a delete req to the logout path
    # and verify that the user is logged out and redirected to the root url. We
    # also check that the login link reapepears and that the logout and profile
    # links disappear.
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    # Simulate a user clicking logout in a second window.
    delete logout_path
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path,      count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
  end

	test "login with remembering" do
		log_in_as(@user, remember_me: '1')
		assert_not_nil cookies['remember_token']
	end

	test "login without remembering" do
		log_in_as(@user, remember_me: '0')
		assert_nil cookies['remember_token']
	end
end
