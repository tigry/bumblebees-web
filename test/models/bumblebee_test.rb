require 'test_helper'

class BumblebeeTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    @bumblebee = @user.bumblebees.build(description: "Lorem ipsum", sound: "/upload/file.mp3")
  end

  test "should be valid" do
    assert @bumblebee.valid?
  end

  test "user id should be present" do
    @bumblebee.user_id = nil
    assert_not @bumblebee.valid?
  end

  test "order should be most recent first" do
    assert_equal Bumblebee.first, bumblebees(:most_recent)
  end

#   test "sound should be present" do

  # test "content should be present " do
  #   @bumblebee.content = "   "
  #   assert_not @bumblebee.valid?
  # end
  #
  # test "content should be at most 140 characters" do
  #   @bumblebee.content = "a" * 141
  #   assert_not @bumblebee.valid?
  # end
end
