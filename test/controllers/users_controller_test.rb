require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  def setup
    @user       = users(:michael)
    @other_user = users(:archer)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  # Because the before filter operates on per-action basis we put 
  # tests in the Users controlelr test.
  test "should redirect edit when not logged in" do
    get :edit, id: @user
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch :update, id: @user, user: { name: @user.name, email: @user.email }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  # We expect useres to redirect to root path instead of the login path
  # because a user trying to edit a different user would already be logged in
  test "should redirect edit when logged in as wrong user" do
    log_in_as(@other_user)
    get :edit, id: @user
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect update when logged in as wrong user" do
    log_in_as(@other_user)
    patch :update, id: @user, user: { name: @user.name, email: @user.email }
    assert flash.empty?
    assert_redirected_to root_url
  end

   test "should redirect index when not logged in" do
    get :index
    assert_redirected_to login_url
  end

   # Testing deleting users
   # 1) Users who aren't logged in should be redirected to the login page
   test "should redirect destroy when not logged in" do
     assert_no_difference 'User.count' do
       delete :destroy, id: @user
     end
     assert_redirected_to login_url
   end

   # 2) Users who are logged in but who aren't admins should be redirected
   # to home page
   test "should redirect destroy when logged in as a non-admin" do
     log_in_as(@other_user)
     assert_no_difference 'User.count' do
       delete :destroy, id: @user
     end
     assert_redirected_to root_url
   end
end
