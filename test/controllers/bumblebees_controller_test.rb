require 'test_helper'

class BumblebeesControllerTest < ActionController::TestCase

  def setup
    @bumblebee = bumblebees(:orange)
  end

  # Because we access bumblebees trough their associated users,
  # all actions must require users to be logged in
  test "should redirect create when not logged in" do
    assert_no_difference 'Bumblebee.count' do
      post :create, bumblebee: { content: "Lorem ipsum" }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Bumblebee.count' do
      delete :destroy, id: @bumblebee
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy for wrong bumblebee" do
    log_in_as(users(:michael))
    bumblebee = bumblebees(:ants)
    assert_no_difference 'Bumblebee.count' do
      delete :destroy, id: bumblebee
    end
    assert_redirected_to root_url
  end
end
