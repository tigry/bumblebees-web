class User
  attr_accessor :name, :email # create getters and setters

  def initialize(attributes = {}) # method when we execute User.new, with one argument 'attributes'
    @name = attributes[:name]
    @email = attributes[:email]
  end

  def formatted_email # :akes name and email and creates name name@email format
    "#{@name} <#{@email}>"
  end
end
