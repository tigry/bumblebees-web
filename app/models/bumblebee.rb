class Bumblebee < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order(created_at: :desc)  }
  mount_uploader :picture, PictureUploader
  mount_uploader :sound, SoundUploader
  validates :user_id,     presence: true
  #validates :description, presence: true
  # validates :sound,       presence: true
  validate  :picture_size, :sound_size

  private

    # Validates the size of an uploaded picture.
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end

    def sound_size
      if sound.size > 5.megabytes
        errors.add(:sound, "Should be less than 5MB")
      end
    end
end
