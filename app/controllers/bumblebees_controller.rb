class BumblebeesController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: [:destroy, :edit]

  def create
    @bumblebee = current_user.bumblebees.build(bumblebee_params)
    if @bumblebee.save
      flash[:success] = "Bumblebee created!"
      redirect_to root_url
    else
      # Failed submission currently break - we suppress the feed entirely by 
      # assigning it an empty array
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def destroy
    @bumblebee.destroy
    flash[:success] = "Bumblebee deleted"
    redirect_to request.referrer || root_url
  end

  def edit
    @bumblebee = if current_user.admin?
      Bumblebee.find(params[:id])
    else
      current_user.bumblebees.find_by(id: params[:id])
    end
  end

  def update
    @bumblebee = if current_user.admin?
      Bumblebee.find(params[:id])
    else
      current_user.bumblebees.find_by(id: params[:id])
    end
    if @bumblebee.update_attributes(bumblebee_params)
      # Handle a successful update.
      flash[:success] = "Bumblebee updated"
      redirect_to root_url 
    else 
      flash[:warning] = "Bumblebee not updated"
    end
  end

  def unconfirmed
    if logged_in? && user_admin?
      # @bumblebee = current_user.bumblebees.build 
      # @feed_items = current_user.feed.paginate(page: params[:page])
      @feed_items = Bumblebee.all.where(approved: nil).paginate(page: params[:page])
    end
  end

  def index
    @feed_items = Bumblebee.all.where(approved: true).paginate(page: params[:page])
    @bumblebees = Bumblebee.all.where(approved: true)
    respond_to do |format|
      format.html
      format.json { render json: @bumblebees.as_json(only: [:id, :species, :gender_type, :sound, :image]) }
    end
  end

  def show
    @bumblebee = Bumblebee.find(params[:id])
    respond_to do |format|
      format.html
      format.json { render json: @bumblebee.as_json(only: [:id, :species, :gender_type, :sound]) }
    end
  end
  private

    def bumblebee_params
      params.require(:bumblebee).permit(:description, :picture, :species,
                                        :gender_type, :sound, :approved)

    end

    def correct_user
      @bumblebee = if current_user.admin?
        Bumblebee.find(params[:id])
      else
        current_user.bumblebees.find_by(id: params[:id])
      end
      redirect_to root_url if @bumblebee.nil?
    end
end
