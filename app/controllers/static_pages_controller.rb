class StaticPagesController < ApplicationController
  protect_from_forgery with: :exception
  before_action :detect_browser


  require 'rest-client'
  API_BASE_URL = "http://212.235.208.133:9001/classify"
  
  def classify
#    RestClient.post('http://212.235.208.133:9001/classify', :audio => File.new('owl_call.wav'))
    # @response = RestClient.post('http://212.235.208.133:9001/classify', :audio => File.new('owl_call.wav'))
    puts "test"
  end

  def home
    if logged_in? 
      @bumblebee = current_user.bumblebees.build 
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end

  def help
  end

  def about
  end

  def contact
  end

  private
    def detect_browser
      case request.user_agent
        when /iPad/i
          request.variant = :tablet
          render layout: "mobile_layout"
        when /iPhone/i
          request.variant = :phone
          render layout: "mobile_layout"
        when /Android/i && /mobile/i
          request.variant = :phone
          render layout: "mobile_layout"
        when /Android/i
          request.variant = :tablet
          render layout: "mobile_layout"
        when /Windows Phone/i
          request.variant = :phone
          render layout: "mobile_layout"
        else
          request.variant = :desktop
      end
  end
end
