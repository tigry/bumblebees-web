module Api
  module V1
    class BumblebeesController < ApplicationController
      before_action :doorkeeper_authorize! # Require access token for all actions
      respond_to :json

      def index
        respond_with current_user.bumblebees
        # respond_with Bumblebee.all.where(approved: true)
      end

      def show
        respond_with Bumblebee.find(params[:id])
      end

      def create
        respond_with Bumblebee.create(params[:bumblebee])
      end

      def update
        respond_with Bumblebee.update(params[:id], params[:bumblebee])
      end

      def destroy
        respond_with Bumblebee.destroy(params[:id])
      end

      private 

        def current_user
          @current_user ||= User.find(doorkeeper_token.resource_owner_id)
        end
    end
  end
end
