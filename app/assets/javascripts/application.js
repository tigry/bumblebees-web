// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require turbolinks
//= require_tree .


/* Disable submit button if input file was not selected */
$(document).ready(function(){
    $('.submit-button').attr('disabled',true);
    $('#file').keyup(function(){
        if($(this).val().length !=0)
            $('.submit-button').attr('disabled', false);            
        else
            $('.submit-button').attr('disabled',true);
    })
});

/*
//Program a custom submit function for the form
$("form#data").submit(function(event){
 
  //disable the default form submission
  event.preventDefault();
 
  //grab all form data  
  var formData = new FormData($(this)[0]);
 
  $.ajax({
    url: 'http://212.235.208.133:9001/classify',
    mthod: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    success: function (returndata) {
      alert(returndata);
    }
  });
 
  return false;
});
*/

/*
var formData = $("#myform").serializeArray();
var URL = $("#myform").attr("action");
$.post(URL,
    formData,
    function(data, textStatus, jqXHR)
    {
       //data: Data from server.    
    }).fail(function(jqXHR, textStatus, errorThrown) 
    {
 
    });

$( '#my-form' )
  .submit( function( e ) {
    $.ajax( {
      url: 'http://212.235.208.133:9001/classify',
      type: 'POST',
      data: new FormData( this ),
      processData: false,
      contentType: false
    } );
    e.preventDefault();
  } );

/*
$( "#classify" ).submit(function( event ) {
  alert( "Handler for .submit() called." );
//  event.preventDefault();
  event.submit()
});
*/

/*
var i = 0;

function log(s) {
    $('#log').val($('#log').val() + '\n' + (++i) + ': ' + s);
}

log('before ajax call');

//setup the event handler for the button
$('#action-classify').click(function() {
    alert('do something');
});


//disable the button for now (this will be enabled when the data is received)
//$('#submit-button').attr('disabled', true);



/*
$.ajax({
    type: "POST",
    url: 'http://212.235.208.133:9001/classify',
    success: function(data) {
        log('in success callback');
        log('received data: ' + data);
        //enable the button
        $('#submit-button').attr('disabled', false);
    }
});
*/

/*
log('after ajax call');

$(document).ready(function() {
    log('document ready');
    setTimeout(function() {
        log('timeout');
    }, 1000);
});
*/
