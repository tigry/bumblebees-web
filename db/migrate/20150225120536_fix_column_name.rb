class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :bumblebees, :type, :gender_type
  end
end
