class CreateBumblebees < ActiveRecord::Migration
  def change
    create_table :bumblebees do |t|
      t.text :description
      t.string :species
      t.integer :type
      t.string :picture
      t.string :sound
      t.boolean :approved
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :bumblebees, :users
    # For retrieving bumblebees associated with given user id in reverse order
    # adds an index on the user_id and created_at columns
    add_index :bumblebees, [:user_id, :created_at]
  end
end
